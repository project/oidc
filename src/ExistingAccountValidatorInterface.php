<?php

namespace Drupal\oidc;

use Drupal\user\UserInterface;

/**
 * Interface for the existing account validator service.
 */
interface ExistingAccountValidatorInterface {

  /**
   * Check if an existing account may be linked to an external user.
   *
   * @param \Drupal\user\UserInterface $account
   *   The account to validate.
   *
   * @return bool
   *   TRUE if the account is valid.
   */
  public function isValid(UserInterface $account);

  /**
   * Reset the cached validation results.
   *
   * @param \Drupal\user\UserInterface $account
   *   If specified, clear only the cache for this account.
   */
  public function resetCache(?UserInterface $account = NULL);

}
