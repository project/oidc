<?php

namespace Drupal\oidc\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\oidc\OpenidConnectSessionInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Access check for the OpenID Connect redirect routes.
 */
class OpenidConnectRedirectAccessCheck implements AccessInterface {

  /**
   * The OpenID Connect session service.
   *
   * @var \Drupal\oidc\OpenidConnectSessionInterface
   */
  protected $session;

  /**
   * Class constructor.
   *
   * @param \Drupal\oidc\OpenidConnectSessionInterface $session
   *   The OpenID Connect session service.
   */
  public function __construct(OpenidConnectSessionInterface $session) {
    $this->session = $session;
  }

  /**
   * Perform the access check.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   * @param \Drupal\Core\Session\AccountProxyInterface $account
   *   Account to run the access check for.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   The access check result.
   */
  public function access(Request $request, AccountProxyInterface $account) {
    if ($account->isAuthenticated()) {
      return AccessResult::forbidden();
    }

    $state = $this->session->getState();

    if ($state === NULL || $state !== $request->query->get('state')) {
      return AccessResult::forbidden();
    }

    return AccessResult::allowed();
  }

}
