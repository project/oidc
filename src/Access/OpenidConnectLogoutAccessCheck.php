<?php

namespace Drupal\oidc\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\oidc\OpenidConnectSessionInterface;

/**
 * Access check for the OpenID Connect logout route.
 */
class OpenidConnectLogoutAccessCheck implements AccessInterface {

  /**
   * The OpenID Connect session service.
   *
   * @var \Drupal\oidc\OpenidConnectSessionInterface
   */
  protected $session;

  /**
   * Class constructor.
   *
   * @param \Drupal\oidc\OpenidConnectSessionInterface $session
   *   The OpenID Connect session service.
   */
  public function __construct(OpenidConnectSessionInterface $session) {
    $this->session = $session;
  }

  /**
   * Perform the access check.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   The access check result.
   */
  public function access() {
    return AccessResult::allowedIf($this->session->isAuthenticated());
  }

}
