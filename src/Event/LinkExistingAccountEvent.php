<?php

namespace Drupal\oidc\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\oidc\JsonWebTokens;
use Drupal\user\UserInterface;

/**
 * Allows linking an existing account to the authenticating user.
 */
class LinkExistingAccountEvent extends Event {

  /**
   * The external authentication provider name.
   *
   * @var string
   */
  protected $provider;

  /**
   * The JSON web tokens.
   *
   * @var \Drupal\oidc\JsonWebTokens
   */
  protected $tokens;

  /**
   * The existing account to link.
   *
   * @var \Drupal\user\UserInterface|null
   */
  protected $account = NULL;

  /**
   * Class constructor.
   *
   * @param string $provider
   *   The external authentication provider name.
   * @param \Drupal\oidc\JsonWebTokens $tokens
   *   The JSON web tokens.
   */
  public function __construct($provider, JsonWebTokens $tokens) {
    $this->provider = $provider;
    $this->tokens = $tokens;
  }

  /**
   * Get the name of the external authentication provider.
   *
   * @return string
   *   The external authentication provider name.
   */
  public function getProvider() {
    return $this->provider;
  }

  /**
   * Get the JSON web tokens.
   *
   * @return \Drupal\oidc\JsonWebTokens
   *   The JSON web tokens.
   */
  public function getJsonWebTokens() {
    return $this->tokens;
  }

  /**
   * Get the existing account (if applicable).
   *
   * @return \Drupal\user\UserInterface|null
   *   The existing account to link or NULL if not set.
   */
  public function getAccount() {
    return $this->account;
  }

  /**
   * Set the existing account (if applicable).
   *
   * @param \Drupal\user\UserInterface $account
   *   The existing account to link.
   */
  public function setAccount(UserInterface $account) {
    $this->account = $account;
  }

}
