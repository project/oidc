<?php

namespace Drupal\oidc;

/**
 * Represents a set of JSON web tokens.
 */
class JsonWebTokens implements ArrayTransformationInterface {

  /**
   * The type.
   *
   * @var string
   */
  protected $type;

  /**
   * The id token.
   *
   * @var \Drupal\oidc\Token
   */
  protected $idToken;

  /**
   * The access token.
   *
   * @var \Drupal\oidc\Token
   */
  protected $accessToken;

  /**
   * The refresh token.
   *
   * @var \Drupal\oidc\Token|null
   */
  protected $refreshToken;

  /**
   * Associative array of claims.
   *
   * @var array
   */
  protected $claims = [];

  /**
   * Name of the ID claim.
   *
   * @var string
   */
  protected $idClaim = 'sub';

  /**
   * Name of the username claim.
   *
   * @var string
   */
  protected $usernameClaim = 'preferred_username';

  /**
   * Name of the email claim.
   *
   * @var string
   */
  protected $emailClaim = 'email';

  /**
   * Name of the given name claim.
   *
   * @var string
   */
  protected $givenNameClaim = 'given_name';

  /**
   * Name of the family name claim.
   *
   * @var string
   */
  protected $familyNameClaim = 'family_name';

  /**
   * Class constructor.
   *
   * @param string $type
   *   The type.
   * @param \Drupal\oidc\Token $id_token
   *   The id token.
   * @param \Drupal\oidc\Token $access_token
   *   The access token.
   * @param \Drupal\oidc\Token|null $refresh_token
   *   The refresh token.
   */
  public function __construct($type, Token $id_token, Token $access_token, ?Token $refresh_token = NULL) {
    $this->type = $type;
    $this->idToken = $id_token;
    $this->accessToken = $access_token;
    $this->refreshToken = $refresh_token;
  }

  /**
   * Get the type.
   *
   * @return string
   *   The type.
   */
  public function getType() {
    return $this->type;
  }

  /**
   * Get the id token.
   *
   * @return \Drupal\oidc\Token
   *   The id token
   */
  public function getIdToken() {
    return $this->idToken;
  }

  /**
   * Set the id token.
   *
   * @param \Drupal\oidc\Token $token
   *   The id token.
   *
   * @return $this
   */
  public function setIdToken(Token $token) {
    $this->idToken = $token;

    return $this;
  }

  /**
   * Get the access token.
   *
   * @return \Drupal\oidc\Token
   *   The access token
   */
  public function getAccessToken() {
    return $this->accessToken;
  }

  /**
   * Set the access token.
   *
   * @param \Drupal\oidc\Token $token
   *   The access token.
   *
   * @return $this
   */
  public function setAccessToken(Token $token) {
    $this->accessToken = $token;

    return $this;
  }

  /**
   * Get the refresh token.
   *
   * @return \Drupal\oidc\Token
   *   The refresh token
   */
  public function getRefreshToken() {
    return $this->refreshToken;
  }

  /**
   * Set the refresh token.
   *
   * @param \Drupal\oidc\Token $token
   *   The refresh token.
   *
   * @return $this
   */
  public function setRefreshToken(Token $token) {
    $this->refreshToken = $token;

    return $this;
  }

  /**
   * Clear the refresh token.
   *
   * @return $this
   */
  public function clearRefreshToken() {
    $this->refreshToken = NULL;

    return $this;
  }

  /**
   * Get an associative array of all claims.
   *
   * @return array
   *   The claims.
   */
  public function getClaims() {
    return $this->claims;
  }

  /**
   * Get a single claim.
   *
   * @param string $claim
   *   The claim.
   *
   * @return string|int|array|null
   *   The claim value or NULL if missing.
   */
  public function getClaim($claim) {
    if (array_key_exists($claim, $this->claims)) {
      return $this->claims[$claim];
    }

    return NULL;
  }

  /**
   * Set a claim.
   *
   * @param string $claim
   *   The claim.
   * @param string|int|array $value
   *   The claim value.
   *
   * @return $this
   */
  public function setClaim($claim, $value) {
    $this->claims[$claim] = $value;

    return $this;
  }

  /**
   * Get the ID claim.
   *
   * @return int|string|null
   *   The ID or NULL if missing.
   */
  public function getId() {
    return $this->getClaim($this->idClaim);
  }

  /**
   * Get the username claim.
   *
   * @return int|string|null
   *   The username or NULL if missing.
   */
  public function getUsername() {
    return $this->getClaim($this->usernameClaim);
  }

  /**
   * Get the email claim.
   *
   * @return int|string|null
   *   The email address or NULL if missing.
   */
  public function getEmail() {
    return $this->getClaim($this->emailClaim);
  }

  /**
   * Get the given name claim.
   *
   * @return int|string|null
   *   The given name or NULL if missing.
   */
  public function getGivenName() {
    return $this->getClaim($this->givenNameClaim);
  }

  /**
   * Get the family name claim.
   *
   * @return int|string|null
   *   The family name or NULL if missing.
   */
  public function getFamilyName() {
    return $this->getClaim($this->familyNameClaim);
  }

  /**
   * Set the name of the ID claim.
   *
   * @param string $claim
   *   The claim name.
   *
   * @return $this
   */
  public function setIdClaim($claim) {
    $this->idClaim = $claim;

    return $this;
  }

  /**
   * Set the name of the username claim.
   *
   * @param string $claim
   *   The claim name.
   *
   * @return $this
   */
  public function setUsernameClaim($claim) {
    $this->usernameClaim = $claim;

    return $this;
  }

  /**
   * Set the name of the email address claim.
   *
   * @param string $claim
   *   The claim name.
   *
   * @return $this
   */
  public function setEmailClaim($claim) {
    $this->emailClaim = $claim;

    return $this;
  }

  /**
   * Set the name of the given name claim.
   *
   * @param string $claim
   *   The claim name.
   *
   * @return $this
   */
  public function setGivenNameClaim($claim) {
    $this->givenNameClaim = $claim;

    return $this;
  }

  /**
   * Set the name of the family name claim.
   *
   * @param string $claim
   *   The claim name.
   *
   * @return $this
   */
  public function setFamilyNameClaim($claim) {
    $this->familyNameClaim = $claim;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function toArray() {
    return [
      'type' => $this->type,
      'id_token' => $this->idToken->toArray(),
      'access_token' => $this->accessToken->toArray(),
      'refresh_token' => $this->refreshToken ? $this->refreshToken->toArray() : NULL,
      'claims' => $this->claims,
      'id_claim' => $this->idClaim,
      'username_claim' => $this->usernameClaim,
      'email_claim' => $this->emailClaim,
      'given_name_claim' => $this->givenNameClaim,
      'family_name_claim' => $this->familyNameClaim,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function fromArray(array $data) {
    // Backwards compatibility, support the old format with string tokens.
    if (is_string($data['id_token'])) {
      $data['id_token'] = ['value' => $data['id_token'], 'expires' => $data['expires']];
      $data['access_token'] = ['value' => $data['access_token'], 'expires' => $data['expires']];
    }

    $instance = new static(
      $data['type'],
      Token::fromArray($data['id_token']),
      Token::fromArray($data['access_token'])
    );

    if (isset($data['refresh_token'])) {
      $instance->setRefreshToken(Token::fromArray($data['refresh_token']));
    }

    $instance->claims = $data['claims'];
    $instance->idClaim = $data['id_claim'];
    $instance->usernameClaim = $data['username_claim'];
    $instance->emailClaim = $data['email_claim'];
    $instance->givenNameClaim = $data['given_name_claim'];
    $instance->familyNameClaim = $data['family_name_claim'];

    return $instance;
  }

}
