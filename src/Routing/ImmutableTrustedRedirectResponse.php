<?php

namespace Drupal\oidc\Routing;

use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Url;

/**
 * Provides a redirect response to a trusted URL which can not be changed once the object has been created.
 */
class ImmutableTrustedRedirectResponse extends TrustedRedirectResponse {

  /**
   * Class constructor.
   *
   * @param string|Url $url
   *   The URL to redirect to. The URL should be an object or an absolute URL.
   * @param int $status
   *   The status code (302 by default).
   * @param array $headers
   *   The additional headers (Location is always set to the given URL).
   */
  public function __construct($url, $status = 302, array $headers = []) {
    if ($url instanceof Url) {
      $url = $url->toString(TRUE)->getGeneratedUrl();
    }

    parent::__construct($url, $status, $headers);
  }

  /**
   * {@inheritdoc}
   */
  public function setTargetUrl($url): static {
    if (empty($this->targetUrl) || $this->targetUrl === $url) {
      parent::setTargetUrl($url);
    }

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setTrustedTargetUrl($url) {
    if (empty($this->targetUrl) || $this->targetUrl === $url) {
      parent::setTrustedTargetUrl($url);
    }

    return $this;
  }

}
