<?php

namespace Drupal\oidc;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceModifierInterface;

/**
 * Service provider used to alter the existing services.
 */
class OidcServiceProvider implements ServiceModifierInterface {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    // Set the cookie lifetime.
    $options = $container->getParameter('session.storage.options');
    $options['cookie_lifetime'] = 0;

    $container->setParameter('session.storage.options', $options);
  }

}
