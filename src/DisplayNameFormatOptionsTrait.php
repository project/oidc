<?php

namespace Drupal\oidc;

use Drupal\Core\Utility\Token;

/**
 * Trait to build the display name format options.
 */
trait DisplayNameFormatOptionsTrait {

  /**
   * The token service.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * Set the token service.
   *
   * @param \Drupal\Core\Utility\Token $token
   *   The token service.
   *
   * @return $this
   */
  public function setTokenService(Token $token) {
    $this->token = $token;

    return $this;
  }

  /**
   * Get the token service.
   *
   * @return \Drupal\Core\Utility\Token
   *   The token service.
   */
  protected function getTokenService() {
    if ($this->token === NULL) {
      $this->token = \Drupal::service('token');
    }

    return $this->token;
  }

  /**
   * Build the display name format options.
   *
   * @param array $formats
   *   The user token based display name formats.
   *
   * @return array
   *   Associative array of the human readable display name formats
   *   keyed by their token based format.
   */
  protected function buildDisplayNameFormatOptions(array $formats) {
    // Get a list of all used tokens.
    $tokens = implode(' ', $formats);
    $tokens = $this->getTokenService()->scan($tokens);

    if (!isset($tokens['user'])) {
      return $formats;
    }

    // Get the user tokens.
    $info = $this->getTokenService()->getInfo();
    if (!isset($info['tokens']['user'])) {
      return $formats;
    }

    $info = $info['tokens']['user'];

    // Build the replacements array.
    $replacements = [];
    foreach ($tokens['user'] as $name => $token) {
      if (isset($info[$name]['name'])) {
        $replacements[$token] = '<' . $info[$name]['name'] . '>';
      }
    }

    if (!$replacements) {
      return $formats;
    }

    // Build and return the options array.
    $options = [];
    foreach ($formats as $format) {
      $options[$format] = strtr($format, $replacements);
    }

    return $options;
  }

}
