<?php

namespace Drupal\oidc;

/**
 * Interface for objects that can be transformed to and from an array.
 */
interface ArrayTransformationInterface {

  /**
   * Get the array representation of this object.
   *
   * @return array
   *   The object properties as array.
   */
  public function toArray();

  /**
   * Create an instance from an array.
   *
   * @param array $data
   *   The array containing all object properties.
   *
   * @return static
   *   The class instance.
   */
  public static function fromArray(array $data);

}
