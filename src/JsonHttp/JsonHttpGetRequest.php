<?php

namespace Drupal\oidc\JsonHttp;

/**
 * Represents a JSON HTTP GET request.
 */
class JsonHttpGetRequest extends JsonHttpRequest implements JsonHttpGetRequestInterface {

}
