<?php

namespace Drupal\oidc\JsonHttp;

/**
 * Interface for a JSON HTTP GET request.
 */
interface JsonHttpGetRequestInterface extends JsonHttpRequestInterface {

}
