<?php

namespace Drupal\oidc\JsonHttp;

/**
 * Provides an interface for the JSON HTTP client service.
 */
interface JsonHttpClientInterface {

  /**
   * Perform an HTTP GET request.
   *
   * @param \Drupal\oidc\JsonHttp\JsonHttpGetRequestInterface $request
   *   The JSON HTTP GET request.
   *
   * @return array
   *   The decoded JSON response.
   */
  public function get(JsonHttpGetRequestInterface $request);

  /**
   * Perform an HTTP POST request.
   *
   * @param \Drupal\oidc\JsonHttp\JsonHttpPostRequestInterface $request
   *   The JSON HTTP POST request.
   *
   * @return array
   *   The decoded JSON response.
   */
  public function post(JsonHttpPostRequestInterface $request);

}
