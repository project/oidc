<?php

namespace Drupal\oidc\JsonHttp;

/**
 * Base class to represent a JSON HTTP request.
 */
abstract class JsonHttpRequest implements JsonHttpRequestInterface {

  /**
   * The request URL.
   *
   * @var string
   */
  protected $url;

  /**
   * An associative array of request headers.
   *
   * @var array
   */
  protected $headers = [];

  /**
   * The credentials for basic authentication.
   *
   * @var array|null
   */
  protected $credentials;

  /**
   * Class constructor.
   *
   * @param string $url
   *   The request URL.
   */
  public function __construct($url) {
    $this->url = $url;
  }

  /**
   * Creates a request instance.
   *
   * @param string $url
   *   The request URL.
   *
   * @return static
   */
  public static function create($url) {
    return new static($url);
  }

  /**
   * {@inheritdoc}
   */
  public function getUrl() {
    return $this->url;
  }

  /**
   * {@inheritdoc}
   */
  public function getHeaders() {
    return $this->headers;
  }

  /**
   * Add a request header.
   *
   * @param string $name
   *   The header name.
   * @param string $value
   *   The header value.
   *
   * @return $this
   */
  public function addHeader($name, $value) {
    $this->headers[$name] = $value;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getBasicAuth() {
    return $this->credentials;
  }

  /**
   * Set the credentials for basic authentication.
   *
   * @param string $username
   *   The username.
   * @param string $password
   *   The password.
   *
   * @return $this
   */
  public function setBasicAuth($username, $password) {
    $this->credentials = [$username, $password];

    return $this;
  }

}
