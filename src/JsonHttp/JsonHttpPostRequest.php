<?php

namespace Drupal\oidc\JsonHttp;

/**
 * Represents a JSON HTTP POST request.
 */
class JsonHttpPostRequest extends JsonHttpRequest implements JsonHttpPostRequestInterface {

  /**
   * An associative array of form parameters.
   *
   * @var array|null
   */
  protected $formParams;

  /**
   * {@inheritdoc}
   */
  public function getFormParams() {
    return $this->formParams;
  }

  /**
   * Add a form parameter.
   *
   * @param string $name
   *   The form parameter.
   * @param string $value
   *   The value.
   *
   * @return $this
   */
  public function addFormParameter($name, $value) {
    $this->formParams[$name] = $value;

    return $this;
  }

}
