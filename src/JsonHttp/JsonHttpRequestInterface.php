<?php

namespace Drupal\oidc\JsonHttp;

/**
 * Interface for a JSON HTTP request.
 */
interface JsonHttpRequestInterface {

  /**
   * Get the request URL.
   *
   * @return string
   *   The request URL.
   */
  public function getUrl();

  /**
   * Get an associative array of HTTP headers.
   *
   * @return array
   *   The HTTP headers.
   */
  public function getHeaders();

  /**
   * Get the credentials for basic authentication.
   *
   * @return array|null
   *   If set, an indexed array with the username at index 0 and the password at index 1.
   */
  public function getBasicAuth();

}
