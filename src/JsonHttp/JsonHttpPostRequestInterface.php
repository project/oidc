<?php

namespace Drupal\oidc\JsonHttp;

/**
 * Interface for a JSON HTTP POST request.
 */
interface JsonHttpPostRequestInterface extends JsonHttpGetRequestInterface {

  /**
   * Get an associative array of form parameters.
   *
   * @return array|null
   *   The form parameters or NULL if not set.
   */
  public function getFormParams();

}
