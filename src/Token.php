<?php

namespace Drupal\oidc;

/**
 * Represents a token.
 */
class Token implements ArrayTransformationInterface {

  /**
   * The token value.
   *
   * @var string
   */
  protected $value;

  /**
   * Unix timestamp when the token expires.
   *
   * @var int
   */
  protected $expires;

  /**
   * Class constructor.
   *
   * @param string $value
   *   The token value.
   * @param int $expires
   *   Unix timestamp when the token expires.
   */
  public function __construct($value, $expires) {
    $this->value = $value;
    $this->expires = $expires;
  }

  /**
   * Get the value.
   *
   * @return string
   *   The token value.
   */
  public function getValue() {
    return $this->value;
  }

  /**
   * Get the expiration timestamp.
   *
   * @return int
   *   Unix timestamp when the token expires.
   */
  public function getExpires() {
    return $this->expires;
  }

  /**
   * {@inheritdoc}
   */
  public function toArray() {
    return [
      'value' => $this->value,
      'expires' => $this->expires,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function fromArray(array $data) {
    return new static(
      $data['value'],
      $data['expires']
    );
  }

}
