<?php

namespace Drupal\oidc\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteBuilderInterface;
use Drupal\Core\Url;
use Drupal\oidc\OpenidConnectRealm\OpenidConnectRealmManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form to edit the settings.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The OpenID Connect realm manager.
   *
   * @var \Drupal\oidc\OpenidConnectRealm\OpenidConnectRealmManagerInterface
   */
  protected $realmManager;

  /**
   * The route builder service.
   *
   * @var \Drupal\Core\Routing\RouteBuilderInterface
   */
  protected $routeBuilder;

  /**
   * The login path options.
   *
   * @var array
   */
  protected $loginPathOptions;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory service.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typed_config_manager
   *   The typed config manager.
   * @param \Drupal\Core\Routing\RouteBuilderInterface $route_builder
   *   The route builder service.
   * @param \Drupal\oidc\OpenidConnectRealm\OpenidConnectRealmManagerInterface $realm_manager
   *   The OpenID Connect realm manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, TypedConfigManagerInterface $typed_config_manager, RouteBuilderInterface $route_builder, OpenidConnectRealmManagerInterface $realm_manager) {
    parent::__construct($config_factory, $typed_config_manager);

    $this->realmManager = $realm_manager;
    $this->routeBuilder = $route_builder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('router.builder'),
      $container->get('plugin.manager.openid_connect_realm')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['oidc.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'oidc_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $settings = $this->config('oidc.settings');

    $options = $this->getLoginPathOptions();
    $default = $settings->get('login_path');

    $form['login_path'] = [
      '#type' => 'select',
      '#title' => $this->t('Login page'),
      '#description' => $this->t('The selected page will replace the default login page.'),
      '#options' => $options,
      '#empty_option' => '- ' . $this->t('Default') . ' -',
    ];

    if ($default) {
      if (!isset($options[$default])) {
        $form['login_path']['#default_value'] = '*';
      }
      else {
        $form['login_path']['#default_value'] = $default;
        $default = NULL;
      }
    }

    $form['login_path_custom'] = [
      '#type' => 'path',
      '#description' => $this->t('Path of the custom login page.'),
      '#default_value' => $default,
    ];

    if (count($options) === 1) {
      $form['login_path']['#default_value'] = '*';
      $form['login_path']['#access'] = FALSE;
      $form['login_path_custom']['#title'] = $this->t('Login page');
    }
    else {
      $form['login_path_custom']['#title'] = $this->t('Custom login path');
      $form['login_path_custom']['#title_display'] = 'invisible';
      $form['login_path_custom']['#states'] = [
        'visible' => [
          'select[name="login_path"]' => ['value' => '*'],
        ],
        'required' => [
          'select[name="login_path"]' => ['value' => '*'],
        ],
      ];
    }

    $form['redirect_403'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Redirect 403'),
      '#description' => $this->t('Redirect 403 responses to the login page.'),
      '#default_value' => (int) $settings->get('redirect_403'),
    ];

    $form['disable_user_routes'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable unneeded user routes'),
      '#description' => $this->t('Disables the user routes for registration and password reset.'),
      '#default_value' => (int) $settings->get('disable_user_routes'),
    ];

    $form['show_session_expired_message'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show session expired message'),
      '#description' => $this->t('Show a message when the user is logged out because the session expired.'),
      '#default_value' => (int) $settings->get('show_session_expired_message'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Get a list login path options.
   *
   * @return string[]
   *   The login path options.
   */
  protected function getLoginPathOptions() {
    if ($this->loginPathOptions === NULL) {
      $this->loginPathOptions = [];

      foreach ($this->realmManager->getDefinitions() as $plugin_id => $definition) {
        $this->loginPathOptions['/oidc/login/' . urlencode($plugin_id)] = $this->t('@name login', [
          '@name' => $definition['name'],
        ]);
      }

      $this->loginPathOptions['*'] = $this->t('Custom');
    }

    return $this->loginPathOptions;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->getValue('login_path') !== '*') {
      return;
    }

    if (count($this->getLoginPathOptions()) === 1) {
      return;
    }

    $login_path = $form_state->getValue('login_path_custom');

    if ($login_path === NULL) {
      $form_state->setErrorByName('login_path_custom', $this->t('The custom login path cannot be empty.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $login_path = $form_state->getValue('login_path');

    if ($login_path === '*') {
      $login_path = $form_state->getValue('login_path_custom');

      if ($login_path) {
        $url = Url::fromRoute($login_path['route_name'], $login_path['route_parameters']);
        $login_path = '/' . $url->getInternalPath();
      }
    }

    $this->config('oidc.settings')
      ->set('login_path', $login_path ?: NULL)
      ->set('redirect_403', (bool) $form_state->getValue('redirect_403', 1))
      ->set('disable_user_routes', (bool) $form_state->getValue('disable_user_routes', 0))
      ->set('show_session_expired_message', (bool) $form_state->getValue('show_session_expired_message', 0))
      ->save();

    // Rebuild the routing information in case the registration path permission
    // changed.
    $this->routeBuilder->rebuild();

    parent::submitForm($form, $form_state);
  }

}
