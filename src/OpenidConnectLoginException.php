<?php

namespace Drupal\oidc;

use Drupal\Core\Url;

/**
 * Exception to handle OpenID Connect login failures silently.
 */
class OpenidConnectLoginException extends \RuntimeException {

  /**
   * The destination URL.
   *
   * @var \Drupal\Core\Url
   */
  protected $destination;

  /**
   * Class constructor.
   *
   * @param string $message
   *   The exception message.
   * @param \Drupal\Core\Url|null $destination
   *   The URL to redirect to, defaults to the front page.
   * @param \Throwable|null $previous
   *   The previous throwable used for the exception chaining.
   */
  public function __construct($message = '', ?Url $destination = NULL, ?\Throwable $previous = NULL) {
    parent::__construct($message, 0, $previous);

    $this->destination = $destination ?? Url::fromRoute('<front>');
  }

  /**
   * Get the destination URL.
   *
   * @return \Drupal\Core\Url
   *   The destination URL.
   */
  public function getDestination() {
    return $this->destination;
  }

}
