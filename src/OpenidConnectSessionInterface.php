<?php

namespace Drupal\oidc;

/**
 * Interface for the OpenID Connect session service.
 */
interface OpenidConnectSessionInterface {

  /**
   * Initialize the realm.
   *
   * @param string $plugin_id
   *   The realm plugin ID.
   *
   * @throws \RuntimeException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function initRealm($plugin_id);

  /**
   * Get the realm plugin ID.
   *
   * @return string|null
   *   The plugin ID or NULL if not set.
   */
  public function getRealmPluginId();

  /**
   * Get the realm plugin.
   *
   * @return \Drupal\oidc\OpenidConnectRealm\OpenidConnectRealmInterface|null
   *   The OpenID Connect realm or NULL if not set.
   */
  public function getRealmPlugin();

  /**
   * Returns TRUE if the user is authenticated with OpenID Connect.
   *
   * @return bool
   *   Boolean indicating if the user authenticated with OpenID Connect.
   */
  public function isAuthenticated();

  /**
   * Initialize the cross-request state.
   *
   * @param string|null $destination
   *   An optional destination path to store.
   *
   * @return string
   *   The generated state.
   *
   * @throws \RuntimeException
   */
  public function initState($destination = NULL);

  /**
   * Get the previously initialized state.
   *
   * @return string|null
   *   The state or NULL if not set.
   */
  public function getState();

  /**
   * Clear the state.
   *
   * @return string|null
   *   The destination path specified when initializing the state.
   */
  public function clearState();

  /**
   * Set the tokens.
   *
   * @param \Drupal\oidc\JsonWebTokens $tokens
   *   The retrieved tokens.
   *
   * @throws \RuntimeException
   */
  public function setJsonWebTokens(JsonWebTokens $tokens);

  /**
   * Get the tokens.
   *
   * @return \Drupal\oidc\JsonWebTokens|null
   *   The tokens or NULL if not set.
   */
  public function getJsonWebTokens();

  /**
   * Destroy the session.
   */
  public function destroy();

}
