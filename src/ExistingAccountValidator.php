<?php

namespace Drupal\oidc;

use Drupal\externalauth\AuthmapInterface;
use Drupal\user\UserInterface;

/**
 * Existing account validator service.
 */
class ExistingAccountValidator implements ExistingAccountValidatorInterface {

  /**
   * Validation results cache.
   *
   * @var bool[]
   */
  protected $validationResults = [];

  /**
   * The authentication mapping service.
   *
   * @var \Drupal\externalauth\AuthmapInterface
   */
  protected $authmap;

  /**
   * Class constructor.
   *
   * @param \Drupal\externalauth\AuthmapInterface $authmap
   *   The authentication mapping service.
   */
  public function __construct(AuthmapInterface $authmap) {
    $this->authmap = $authmap;
  }

  /**
   * {@inheritdoc}
   */
  public function isValid(UserInterface $account) {
    $uid = $account->id();

    if (!isset($this->validationResults[$uid])) {
      $this->validationResults[$uid] = $uid > 1 && !$this->authmap->getAll($uid);
    }

    return $this->validationResults[$uid];
  }

  /**
   * {@inheritdoc}
   */
  public function resetCache(?UserInterface $account = NULL) {
    if (!$account) {
      $this->validationResults = [];
      return;
    }

    $uid = $account->id();

    if (isset($this->validationResults[$uid])) {
      unset($this->validationResults[$uid]);
    }
  }

}
