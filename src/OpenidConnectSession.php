<?php

namespace Drupal\oidc;

use Drupal\Component\Utility\Crypt;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\SessionManagerInterface;
use Drupal\oidc\OpenidConnectRealm\OpenidConnectRealmManagerInterface;

/**
 * The OpenID Connect session service.
 */
class OpenidConnectSession implements OpenidConnectSessionInterface {

  /**
   * The OpenID Connect realm manager.
   *
   * @var \Drupal\oidc\OpenidConnectRealm\OpenidConnectRealmManagerInterface
   */
  protected $realmManager;

  /**
   * The session manager service.
   *
   * @var \Drupal\Core\Session\SessionManagerInterface
   */
  protected $sessionManager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The OpenID Connect tokens.
   *
   * @var \Drupal\oidc\JsonWebTokens
   */
  protected $tokens;

  /**
   * Class constructor.
   *
   * @param \Drupal\oidc\OpenidConnectRealm\OpenidConnectRealmManagerInterface $realm_manager
   *   The OpenID Connect realm manager.
   * @param \Drupal\Core\Session\SessionManagerInterface $session_manager
   *   The session manager service.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   */
  public function __construct(OpenidConnectRealmManagerInterface $realm_manager, SessionManagerInterface $session_manager, AccountInterface $current_user) {
    $this->realmManager = $realm_manager;
    $this->sessionManager = $session_manager;
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public function initRealm($plugin_id) {
    if ($this->currentUser->isAuthenticated()) {
      throw new \RuntimeException('The realm cannot be changed once the user is authenticated');
    }

    // Ensure the plugin exists.
    $this->realmManager->getDefinition($plugin_id);

    // Set the plugin ID.
    $this->clearState();
    $this->set('plugin_id', $plugin_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getRealmPluginId() {
    return $this->get('plugin_id');
  }

  /**
   * {@inheritdoc}
   */
  public function getRealmPlugin() {
    if (($plugin_id = $this->getRealmPluginId()) === NULL) {
      return NULL;
    }

    return $this->realmManager->loadInstance($plugin_id);
  }

  /**
   * {@inheritdoc}
   */
  public function isAuthenticated() {
    return $this->currentUser->isAuthenticated() && $this->getJsonWebTokens();
  }

  /**
   * {@inheritdoc}
   */
  public function initState($destination = NULL) {
    if (!$this->getRealmPluginId()) {
      throw new \RuntimeException('The realm must be initialized first');
    }

    // Generate a random state.
    $state = substr(Crypt::randomBytesBase64(20), 0, 20);
    $this->set('state', $state);

    // Save the destination.
    if ($destination !== NULL) {
      $this->set('destination', $destination);
    }

    return $state;
  }

  /**
   * {@inheritdoc}
   */
  public function getState() {
    return $this->get('state');
  }

  /**
   * {@inheritdoc}
   */
  public function clearState() {
    $destination = $this->get('destination');

    $this->remove('state');
    $this->remove('destination');

    return $destination;
  }

  /**
   * {@inheritdoc}
   */
  public function setJsonWebTokens(JsonWebTokens $tokens) {
    if (!$this->getRealmPluginId()) {
      throw new \RuntimeException('The realm must be initialized first');
    }

    $this->tokens = $tokens;
    $this->set('tokens', $tokens->toArray());
  }

  /**
   * {@inheritdoc}
   */
  public function getJsonWebTokens() {
    if (!$this->tokens && $this->has('tokens')) {
      $this->tokens = JsonWebTokens::fromArray($this->get('tokens'));
    }

    return $this->tokens;
  }

  /**
   * {@inheritdoc}
   */
  public function destroy() {
    $this->remove('plugin_id');
    $this->remove('tokens');
    $this->clearState();
    $this->tokens = NULL;
  }

  /**
   * Save a variable in the session.
   *
   * @param string $name
   *   Name of the variable to save.
   * @param mixed $value
   *   The value.
   */
  protected function set($name, $value) {
    $this->sessionManager
      ->getBag('attributes')
      ->getBag()
      ->set('oidc_' . $name, $value);
  }

  /**
   * Check if a variable exists in the session.
   *
   * @param string $name
   *   The name of the variable.
   *
   * @return bool
   *   Wether the variable exists.
   */
  protected function has($name) {
    /** @var \Symfony\Component\HttpFoundation\Session\SessionBagProxy $bag */
    $bag = $this->sessionManager->getBag('attributes');

    if ($bag->isEmpty()) {
      return FALSE;
    }

    /** @var \Symfony\Component\HttpFoundation\Session\Attribute\AttributeBagInterface $attributes_bag */
    $attributes_bag = $bag->getBag();

    return $attributes_bag->has('oidc_' . $name);
  }

  /**
   * Retrieve a variable from the session.
   *
   * @param string $name
   *   Name of the variable to get.
   * @param mixed $default
   *   The default value if missing.
   *
   * @return mixed
   *   The variable value or default.
   */
  protected function get($name, $default = NULL) {
    if (!$this->has($name)) {
      return $default;
    }

    return $this->sessionManager
      ->getBag('attributes')
      ->getBag()
      ->get('oidc_' . $name, $default);
  }

  /**
   * Remove a variable from the session.
   *
   * @param string $name
   *   Name of the variable to remove.
   */
  protected function remove($name) {
    if ($this->has($name)) {
      $this->sessionManager
        ->getBag('attributes')
        ->getBag()
        ->remove('oidc_' . $name);
    }
  }

}
