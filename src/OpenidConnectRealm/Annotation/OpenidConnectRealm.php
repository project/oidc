<?php

namespace Drupal\oidc\OpenidConnectRealm\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines an OpenID Connect realm annotation object.
 *
 * Plugin Namespace: Plugin\OpenidConnectRealm.
 *
 * @Annotation
 */
class OpenidConnectRealm extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The realm name.
   *
   * @var \Drupal\Core\Annotation\Translation|string
   */
  public $name;

}
