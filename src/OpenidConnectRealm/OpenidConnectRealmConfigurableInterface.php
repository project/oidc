<?php

namespace Drupal\oidc\OpenidConnectRealm;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Core\Plugin\PluginFormInterface;

/**
 * Interface for a configurable OpenID Connect realm.
 */
interface OpenidConnectRealmConfigurableInterface extends OpenidConnectRealmInterface, ConfigurableInterface, PluginFormInterface {

}
