<?php

namespace Drupal\oidc\OpenidConnectRealm;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Url;
use Drupal\oidc\Token;

/**
 * Interface for an OpenID Connect realm.
 */
interface OpenidConnectRealmInterface extends PluginInspectionInterface {

  /**
   * Indicates whether the realm is enabled.
   *
   * @return bool
   *   The realm status.
   */
  public function isEnabled();

  /**
   * Get the login URL.
   *
   * @param string $state
   *   A unique state to identify this login attempt.
   * @param Url $redirect_url
   *   The login redirect URL.
   *
   * @return \Drupal\Core\Url
   *   The login URL.
   */
  public function getLoginUrl($state, Url $redirect_url);

  /**
   * Get the JSON web tokens for a login attempt.
   *
   * @param string $state
   *   The state to identify the login attempt.
   * @param string $code
   *   The authorization code.
   *
   * @return \Drupal\oidc\JsonWebTokens
   *   The retrieved JSON web tokens.
   *
   * @throws \RuntimeException
   */
  public function getJsonWebTokensForLogin($state, $code);

  /**
   * Get the JSON web tokens for a refresh.
   *
   * @param \Drupal\oidc\Token $refresh_token
   *   The refresh token.
   *
   * @return \Drupal\oidc\JsonWebTokens
   *   The retrieved JSON web tokens.
   *
   * @throws \RuntimeException
   */
  public function getJsonWebTokensforRefresh(Token $refresh_token);

  /**
   * Get the logout URL.
   *
   * @param \Drupal\oidc\Token $id_token
   *   The id token.
   * @param string $state
   *   A unique state to identify this logout request.
   * @param \Drupal\Core\Url $redirect_url
   *   The logout redirect URL.
   *
   * @return \Drupal\Core\Url
   *   The logout URL.
   */
  public function getLogoutUrl(Token $id_token, $state, Url $redirect_url);

  /**
   * Fetch the JSON web keys set and add any missing keys to the local storage.
   *
   * @return bool
   *   Boolean indicating if the update succeeded.
   */
  public function updateJwks();

  /**
   * Clear the JSON web keys set storage.
   */
  public function clearJwks();

  /**
   * Get the display name format.
   *
   * @return string
   *   The display name in tokens.
   */
  public function getDisplayNameFormat();

}
