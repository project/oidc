<?php

namespace Drupal\oidc\OpenidConnectRealm;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Plugin\PluginBase;
use Drupal\oidc\OpenidConnectRealm\Annotation\OpenidConnectRealm;

/**
 * Provides a manager for OpenID Connect realm plugins.
 */
class OpenidConnectRealmManager extends DefaultPluginManager implements OpenidConnectRealmManagerInterface {

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The loaded plugin instances.
   *
   * @var array
   */
  protected $instances = [];

  /**
   * Class constructor.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler, ConfigFactoryInterface $config_factory) {
    parent::__construct(
      'Plugin/OpenidConnectRealm',
      $namespaces,
      $module_handler,
      OpenidConnectRealmInterface::class,
      OpenidConnectRealm::class
    );

    $this->alterInfo('oidc_openid_connect_realm');
    $this->setCacheBackend($cache_backend, 'oidc_openid_connect_realm_plugins');
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function getAll($provider = NULL) {
    $plugin_ids = [];
    foreach ($this->getDefinitions() as $plugin_id => $definition) {
      if ($provider !== NULL && $definition['provider'] !== $provider) {
        continue;
      }

      $plugin_ids[] = $plugin_id;
    }

    return $plugin_ids;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigurable($provider = NULL) {
    $plugin_ids = [];
    foreach ($this->getDefinitions() as $plugin_id => $definition) {
      if (!in_array(OpenidConnectRealmConfigurableInterface::class, class_implements($definition['class']), TRUE)) {
        continue;
      }

      if ($provider !== NULL && $definition['provider'] !== $provider) {
        continue;
      }

      $plugin_ids[] = $plugin_id;
    }

    return $plugin_ids;
  }

  /**
   * {@inheritdoc}
   */
  public function loadInstance($plugin_id) {
    if (isset($this->instances[$plugin_id])) {
      return $this->instances[$plugin_id];
    }

    $class = $this->getDefinition($plugin_id)['class'];
    $configuration = [];

    // Load the configuration if the plugin class extends
    // \Drupal\oidc\OpenidConnectRealm\OpenidConnectRealmConfigurableInterface.
    if (in_array(OpenidConnectRealmConfigurableInterface::class, class_implements($class), TRUE)) {
      $configuration = $this->configFactory
        ->get($this->configName($plugin_id))
        ->get() ?? [];
    }

    $this->instances[$plugin_id] = $this->createInstance($plugin_id, $configuration);

    return $this->instances[$plugin_id];
  }

  /**
   * {@inheritdoc}
   */
  public function saveInstance(OpenidConnectRealmConfigurableInterface $plugin) {
    $name = $this->configName($plugin->getPluginId());
    $config = $this->configFactory->getEditable($name);

    foreach ($plugin->getConfiguration() as $key => $value) {
      $config->set($key, $value);
    }

    $config->save();
  }

  /**
   * {@inheritdoc}
   */
  public function deleteInstance($plugin_id) {
    unset($this->instances[$plugin_id]);

    $name = $this->configName($plugin_id);
    $this->configFactory->getEditable($name)->delete();
  }

  /**
   * Get the configuration name for a plugin.
   *
   * @param string $plugin_id
   *   The plugin ID.
   *
   * @return string
   *   The configuration name.
   */
  protected function configName($plugin_id) {
    $name = 'oidc.realm.';
    $name .= str_replace(PluginBase::DERIVATIVE_SEPARATOR, '.', $plugin_id);

    return $name;
  }

}
