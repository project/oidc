<?php

namespace Drupal\oidc\OpenidConnectRealm;

use Drupal\Component\Plugin\PluginManagerInterface;

/**
 * Interface for the OpenID Connect realm plugin manager.
 */
interface OpenidConnectRealmManagerInterface extends PluginManagerInterface {

  /**
   * Get the IDs of all plugins.
   *
   * @param string|null $provider
   *   The provider to filter on.
   *
   * @return string[]
   *   Array of plugin IDs.
   */
  public function getAll($provider = NULL);

  /**
   * Get the IDs of all configurable plugins.
   *
   * @param string|null $provider
   *   The provider to filter on.
   *
   * @return string[]
   *   Array of plugin IDs.
   */
  public function getConfigurable($provider = NULL);

  /**
   * Load an OpenID Connect realm plugin.
   *
   * @param string $plugin_id
   *   The plugin ID.
   *
   * @return \Drupal\oidc\OpenidConnectRealm\OpenidConnectRealmInterface
   *   The OpenID Connect realm plugin.
   */
  public function loadInstance($plugin_id);

  /**
   * Saves the configuration of an OpenID Connect realm plugin.
   *
   * @param \Drupal\oidc\OpenidConnectRealm\OpenidConnectRealmConfigurableInterface $plugin
   *   The configurable OpenID Connect realm plugin.
   */
  public function saveInstance(OpenidConnectRealmConfigurableInterface $plugin);

  /**
   * Delete the configuration of an OpenID Connect realm plugin.
   *
   * @param string $plugin_id
   *   The plugin ID.
   */
  public function deleteInstance($plugin_id);

}
