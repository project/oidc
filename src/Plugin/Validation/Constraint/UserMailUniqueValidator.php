<?php

namespace Drupal\oidc\Plugin\Validation\Constraint;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Validation\Plugin\Validation\Constraint\UniqueFieldValueValidator;
use Drupal\externalauth\AuthmapInterface;
use Drupal\oidc\OpenidConnectRealm\OpenidConnectRealmManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Validator\Constraint;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Validator for the UserMailUnique constraint.
 */
class UserMailUniqueValidator extends UniqueFieldValueValidator implements ContainerInjectionInterface {

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   *   The entity field manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\externalauth\AuthmapInterface $authmap
   *   The authentication mapping service.
   * @param \Drupal\oidc\OpenidConnectRealm\OpenidConnectRealmManagerInterface $realmManager
   *   The OpenID Connect realm manager.
   */
  public function __construct(protected EntityFieldManagerInterface $entityFieldManager, protected EntityTypeManagerInterface $entityTypeManager, protected AuthmapInterface $authmap, protected OpenidConnectRealmManagerInterface $realmManager) {

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_field.manager'),
      $container->get('entity_type.manager'),
      $container->get('externalauth.authmap'),
      $container->get('plugin.manager.openid_connect_realm')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function validate($items, Constraint $constraint): void {
    /** @var \Drupal\user\UserInterface $user */
    $user = $items->getEntity();

    if (!$user->isNew()) {
      // Skip the validation for OpenID Connect users.
      $authmap = $this->authmap->getAll($user->id());

      foreach ($authmap as $provider => $authname) {
        if (!str_starts_with($provider, 'oidc:')) {
          continue;
        }

        $plugin_id = mb_substr($provider, 5);
        if ($this->realmManager->hasDefinition($plugin_id)) {
          return;
        }
      }
    }

    parent::validate($items, $constraint);
  }

}
