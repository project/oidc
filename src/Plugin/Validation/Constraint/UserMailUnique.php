<?php

namespace Drupal\oidc\Plugin\Validation\Constraint;

use Drupal\user\Plugin\Validation\Constraint\UserMailUnique as UserUserMailUnique;

/**
 * Overrides the default UserMailUnique constraint.
 */
class UserMailUnique extends UserUserMailUnique {

  /**
   * {@inheritdoc}
   */
  public function validatedBy(): string {
    return UserMailUniqueValidator::class;
  }

}
