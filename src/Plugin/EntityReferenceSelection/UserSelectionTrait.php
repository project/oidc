<?php

namespace Drupal\oidc\Plugin\EntityReferenceSelection;

use Drupal\Core\Database\Query\Condition;
use Drupal\Core\Database\Query\ConditionInterface;
use Drupal\Core\Database\Query\SelectInterface;

/**
 * Provides a trait to replace the username condition with a new OR condition.
 */
trait UserSelectionTrait {

  /**
   * {@inheritdoc}
   */
  public function entityQueryAlter(SelectInterface $query) {
    parent::entityQueryAlter($query);

    $this->alterUsernameCondition($query);
  }

  /**
   * Replace the username condition with a new OR condition.
   *
   * @param \Drupal\Core\Database\Query\ConditionInterface $condition
   *   The query condition to update.
   *
   * @return bool
   *   TRUE if the condition has been altered, FALSE otherwise.
   */
  protected function alterUsernameCondition(ConditionInterface $condition) {
    $conditions = &$condition->conditions();

    foreach ($conditions as $key => $specs) {
      if ($key === '#conjunction') {
        continue;
      }

      if ($specs['field'] === 'users_field_data.name') {
        // Remove this condition.
        unset($conditions[$key]);

        // And replace it with a new OR condition that checks the
        // given and family name as well.
        $or = new Condition('OR');
        $or->condition($specs['field'], $specs['value'], $specs['operator']);
        $or->where("CONCAT_WS(' ', users_field_data.given_name, users_field_data.family_name) LIKE :q", [
          ':q' => $specs['value'],
        ]);
        $condition->condition($or);

        return TRUE;
      }

      // Process all nested conditions.
      if ($specs['field'] instanceof ConditionInterface && $this->alterUsernameCondition($specs['field'])) {
        return TRUE;
      }
    }

    return FALSE;
  }

}
