<?php

namespace Drupal\oidc\Plugin\EntityReferenceSelection;

use Drupal\user\Plugin\EntityReferenceSelection\UserSelection as UserUserSelection;

/**
 * Extends the default user selection plugin.
 */
class UserSelection extends UserUserSelection {

  use UserSelectionTrait;

}
