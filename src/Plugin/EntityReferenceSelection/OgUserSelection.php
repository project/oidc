<?php

namespace Drupal\oidc\Plugin\EntityReferenceSelection;

use Drupal\og\Plugin\EntityReferenceSelection\OgUserSelection as OgOgUserSelection;

/**
 * Extends the og user selection plugin.
 */
class OgUserSelection extends OgOgUserSelection {

  use UserSelectionTrait;

}
