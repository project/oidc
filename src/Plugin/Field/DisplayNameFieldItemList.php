<?php

namespace Drupal\oidc\Plugin\Field;

use Drupal\Core\Field\FieldItemList;

/**
 * Item list class for the display name field.
 */
class DisplayNameFieldItemList extends FieldItemList {

  /**
   * Whether or not the value has been calculated.
   *
   * @var bool
   */
  protected $isCalculated = FALSE;

  /**
   * {@inheritdoc}
   */
  public function __get($name) {
    $this->ensureCalculated();
    return parent::__get($name);
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $this->ensureCalculated();
    return parent::isEmpty();
  }

  /**
   * {@inheritdoc}
   */
  public function getIterator(): \ArrayIterator {
    $this->ensureCalculated();

    return parent::getIterator();
  }

  /**
   * {@inheritdoc}
   */
  public function getValue($include_computed = FALSE) {
    $this->ensureCalculated();
    return parent::getValue();
  }

  /**
   * Calculates the value of the field and sets it.
   */
  protected function ensureCalculated() {
    if (!$this->isCalculated) {
      /** @var \Drupal\user\UserInterface $user */
      $user = $this->getEntity();
      $this->setValue($user->getDisplayName());
      $this->isCalculated = TRUE;
    }
  }

}
