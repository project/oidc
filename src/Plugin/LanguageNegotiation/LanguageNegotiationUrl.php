<?php

namespace Drupal\oidc\Plugin\LanguageNegotiation;

use Drupal\Core\Render\BubbleableMetadata;
use Drupal\language\Plugin\LanguageNegotiation\LanguageNegotiationUrl as LanguageLanguageNegotiationUrl;
use Symfony\Component\HttpFoundation\Request;

/**
 * Overrides the default language negotiation plugin.
 */
class LanguageNegotiationUrl extends LanguageLanguageNegotiationUrl {

  /**
   * {@inheritdoc}
   */
  public function processOutbound($path, &$options = [], ?Request $request = NULL, ?BubbleableMetadata $bubbleable_metadata = NULL) {
    if (str_starts_with($path, '/oidc/')) {
      return $path;
    }

    return parent::processOutbound($path, $options, $request, $bubbleable_metadata);
  }

}
