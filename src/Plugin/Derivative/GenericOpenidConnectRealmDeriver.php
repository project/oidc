<?php

namespace Drupal\oidc\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Retrieves all generic OpenID Connect realm plugin definitions.
 */
class GenericOpenidConnectRealmDeriver extends DeriverBase implements ContainerDeriverInterface {

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    /** @var \Drupal\Core\Config\Schema\Sequence $ids */
    $derivative_ids = $this->configFactory->get('oidc.settings')->get('generic_realms') ?? [];

    $this->derivatives = [];
    foreach ($derivative_ids as $id) {
      $config_name = 'oidc.realm.generic.' . $id;
      $config = $this->configFactory->get($config_name);

      $this->derivatives[$id] = $base_plugin_definition;
      $this->derivatives[$id]['name'] = $config->get('name');
      $this->derivatives[$id]['config_dependencies']['config'] = [
        'oidc.settings',
        $config_name,
      ];
    }

    return parent::getDerivativeDefinitions($base_plugin_definition);
  }

}
