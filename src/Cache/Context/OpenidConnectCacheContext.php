<?php

namespace Drupal\oidc\Cache\Context;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\Context\CacheContextInterface;
use Drupal\oidc\OpenidConnectSessionInterface;

/**
 * Provides the "Authenticated with OpenID Connect" cache context.
 *
 * Used to differentiate the cache if the user is authenticated with OpenID Connect.
 *
 * Cache context ID: 'user.openid_connect'.
 */
class OpenidConnectCacheContext implements CacheContextInterface {

  /**
   * The OpenID Connect session service.
   *
   * @var \Drupal\oidc\OpenidConnectSessionInterface
   */
  protected $session;

  /**
   * Class constructor.
   *
   * @param \Drupal\oidc\OpenidConnectSessionInterface $session
   *   The OpenID Connect session service.
   */
  public function __construct(OpenidConnectSessionInterface $session) {
    $this->session = $session;
  }

  /**
   * {@inheritdoc}
   */
  public static function getLabel() {
    return t('Authenticated with OpenID Connect');
  }

  /**
   * {@inheritdoc}
   */
  public function getContext() {
    if ($this->session->isAuthenticated()) {
      return '1';
    }

    return '0';
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata() {
    return new CacheableMetadata();
  }

}
