<?php

namespace Drupal\oidc\Cache\Context;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\Context\CacheContextInterface;
use Drupal\oidc\OpenidConnectSessionInterface;

/**
 * Provides the "OpenID Connect realm" cache context.
 *
 * Used to differentiate the cache based on the OpenID Connect realm used for authentication.
 *
 * Cache context ID: 'user.openid_connect_realm'.
 */
class OpenidConnectRealmCacheContext implements CacheContextInterface {

  /**
   * The OpenID Connect session service.
   *
   * @var \Drupal\oidc\OpenidConnectSessionInterface
   */
  protected $session;

  /**
   * Class constructor.
   *
   * @param \Drupal\oidc\OpenidConnectSessionInterface $session
   *   The OpenID Connect session service.
   */
  public function __construct(OpenidConnectSessionInterface $session) {
    $this->session = $session;
  }

  /**
   * {@inheritdoc}
   */
  public static function getLabel() {
    return t('OpenID Connect realm');
  }

  /**
   * {@inheritdoc}
   */
  public function getContext() {
    if ($this->session->isAuthenticated()) {
      return $this->session->getRealmPluginId();
    }

    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata() {
    return new CacheableMetadata();
  }

}
