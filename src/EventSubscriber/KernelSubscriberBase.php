<?php

namespace Drupal\oidc\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Url;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Base class for the kernel event subscribers.
 */
abstract class KernelSubscriberBase implements EventSubscriberInterface {

  /**
   * The module settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $settings;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory service.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->settings = $config_factory->get('oidc.settings');
  }

  /**
   * Get the login URL.
   *
   * @param string|null $destination
   *   The destination path.
   *
   * @return \Drupal\Core\Url
   *   URL of the (configured) login page.
   */
  protected function getLoginUrl($destination = NULL) {
    // Get the login path.
    $login_path = $this->settings->get('login_path');

    // Get the options.
    $options = [];
    if ($destination !== NULL && $destination !== '') {
      $options['query']['destination'] = $destination;
    }

    if ($login_path !== NULL) {
      return Url::fromUserInput($login_path, $options);
    }

    return Url::fromRoute('user.login', [], $options);
  }

}
