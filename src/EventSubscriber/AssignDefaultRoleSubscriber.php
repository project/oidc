<?php

namespace Drupal\oidc\EventSubscriber;

use Drupal\externalauth\Event\ExternalAuthEvents;
use Drupal\externalauth\Event\ExternalAuthRegisterEvent;
use Drupal\oidc\OpenidConnectSessionInterface;
use Drupal\oidc\Plugin\OpenidConnectRealm\GenericOpenidConnectRealm;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Event subscriber to assign the default user role on registration.
 */
class AssignDefaultRoleSubscriber implements EventSubscriberInterface {

  /**
   * The OpenID Connect session service.
   *
   * @var \Drupal\oidc\OpenidConnectSessionInterface
   */
  protected $session;

  /**
   * Class constructor.
   *
   * @param \Drupal\oidc\OpenidConnectSessionInterface $session
   *   The OpenID Connect session service.
   */
  public function __construct(OpenidConnectSessionInterface $session) {
    $this->session = $session;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[ExternalAuthEvents::REGISTER][] = ['onRegister', 1000];

    return $events;
  }

  /**
   * Assigns the default user role on registration.
   *
   * @param \Drupal\externalauth\Event\ExternalAuthRegisterEvent $event
   *   The register event.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function onRegister(ExternalAuthRegisterEvent $event) {
    // Only generic realms support this.
    $plugin = $this->session->getRealmPlugin();
    if (!$plugin instanceof GenericOpenidConnectRealm) {
      return;
    }

    // The provider must match the realm.
    $provider = 'oidc:' . $plugin->getPluginId();
    if ($provider !== $event->getProvider()) {
      return;
    }

    // Assign the default role.
    $rid = $plugin->getDefaultRoleId();

    if ($rid !== NULL) {
      $account = $event->getAccount();
      $account->addRole($rid);
      $account->save();
    }
  }

}
