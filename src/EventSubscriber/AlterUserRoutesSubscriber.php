<?php

namespace Drupal\oidc\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Routing\RouteBuildEvent;
use Drupal\Core\Routing\RoutingEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Event subscriber to alter some user routes.
 */
class AlterUserRoutesSubscriber implements EventSubscriberInterface {

  /**
   * The module settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $settings;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory service.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->settings = $config_factory->get('oidc.settings');
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[RoutingEvents::ALTER][] = 'onAlter';

    return $events;
  }

  /**
   * Alters some user routes.
   *
   * @param \Drupal\Core\Routing\RouteBuildEvent $event
   *   The route build event.
   */
  public function onAlter(RouteBuildEvent $event) {
    $collection = $event->getRouteCollection();

    if ($this->settings->get('disable_user_routes')) {
      // Make the register and pass routes inaccessible.
      $collection->get('user.register')
        ->addRequirements([
          '_access' => 'FALSE',
        ]);

      $collection->get('user.pass')
        ->addRequirements([
          '_access' => 'FALSE',
        ]);
    }

    // Change the title callback for some routes.
    $collection->get('entity.user.canonical')
      ->setDefault('_title_callback', 'oidc_user_title');

    $collection->get('entity.user.edit_form')
      ->setDefault('_title_callback', 'oidc_user_title');
  }

}
