OpenID Connect Client
=====================

Allows users to authenticate using OpenID Connect.


## Requirements

* [Externalauth](https://www.drupal.org/project/externalauth)
* [sop/jwx](https://github.com/sop/jwx)


## Installation

First of all download the latest release and its dependencies using `composer require drupal/oidc`.
You can also download it manually, but please make sure all requirements are fulfilled before continuing.

Next, simply enable the module as described in the [the module installation guide](https://www.drupal.org/docs/extending-drupal/installing-modules).


## Configuration

### General

You can override the default login page and enable redirecting of 403 responses at
`/admin/config/people/oidc`.

Please note that if you override the login page, you can always access the default page by appending
`?local` to the URL. So the full path would become `/user/login?local`.

### Realms

Before your users can authenticate via OpenID Connect, you have to configure one or more realms
(also known as providers) at `/admin/config/people/oidc/realms`.

On top of this page you should see the login and logout redirect URLs that should be accepted by the
OpenID Connect authentication server.

Click the `Add generic realm` button and fill out the configuration form. The generic realm plugin
uses the `.well-known` configuration of a realm to discover the available endpoints.

Note: Most cases can be covered with the generic realm plugin, but if needed you can always create your
own plugin. Please see the `Drupal\oidc\Plugin\OpenidConnectRealm\GenericOpenidConnectRealm` class for
an example implementation.

Finalize by saving your changes. You should now be able to login via the presented login URL.
