<?php

namespace Drupal\Tests\oidc\Unit;

use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\SessionManagerInterface;
use Drupal\oidc\JsonWebTokens;
use Drupal\oidc\OpenidConnectRealm\OpenidConnectRealmManager;
use Drupal\oidc\OpenidConnectSession;
use Drupal\Tests\UnitTestCase;
use Prophecy\Argument;
use Symfony\Component\HttpFoundation\Session\Attribute\AttributeBagInterface;

/**
 * Tests the OpenidConnectSession class.
 *
 * @covers \Drupal\oidc\OpenidConnectSession
 */
class OpenidConnectSessionTest extends UnitTestCase {

  /**
   * Tests the getRealmPluginId() method.
   *
   * @covers \Drupal\oidc\OpenidConnectSession::getRealmPluginId
   */
  public function testGetRealmPluginId() {
    $session = new OpenidConnectSession(
      $this->createOpenidConnectRealmManager(),
      $this->createSessionManager(),
      $this->createAccount()
    );

    self::assertEquals('generic', $session->getRealmPluginId());

    $session = new OpenidConnectSession(
      $this->createOpenidConnectRealmManager(),
      $this->createSessionManager([]),
      $this->createAccount()
    );

    self::assertNull($session->getRealmPluginId());
  }

  /**
   * Tests the issAuthenticated() method.
   *
   * @covers \Drupal\oidc\OpenidConnectSession::isAuthenticated
   */
  public function testIsAuthenticated() {
    $session = new OpenidConnectSession(
      $this->createOpenidConnectRealmManager(),
      $this->createSessionManager([
        'plugin_id' => 'generic',
        'tokens' => $this->getTokensArray(),
      ]),
      $this->createAccount()
    );

    self::assertTrue($session->isAuthenticated());

    $session = new OpenidConnectSession(
      $this->createOpenidConnectRealmManager(),
      $this->createSessionManager(),
      $this->createAccount(FALSE)
    );

    self::assertFalse($session->isAuthenticated());
  }

  /**
   * Tests the state methods.
   *
   * @covers \Drupal\oidc\OpenidConnectSession::initState
   * @covers \Drupal\oidc\OpenidConnectSession::getState
   * @covers \Drupal\oidc\OpenidConnectSession::clearState
   */
  public function testState() {
    $session = new OpenidConnectSession(
      $this->createOpenidConnectRealmManager(),
      $this->createSessionManager(),
      $this->createAccount()
    );

    self::assertNull($session->getState());

    $session->initState('https://drupal.org');

    self::assertNotEmpty($session->getState());
    self::assertEquals('https://drupal.org', $session->clearState());
    self::assertNull($session->getState());
    self::assertNull($session->clearState());
  }

  /**
   * Tests the setJsonWebTokens() method.
   *
   * @covers \Drupal\oidc\OpenidConnectSession::setJsonWebTokens
   * @covers \Drupal\oidc\OpenidConnectSession::getJsonWebTokens
   */
  public function testSetJsonWebTokens() {
    $session = new OpenidConnectSession(
      $this->createOpenidConnectRealmManager(),
      $this->createSessionManager(),
      $this->createAccount()
    );

    $tokens = $this->prophesize(JsonWebTokens::class);
    $tokens->toArray()->willReturn([]);
    $tokens = $tokens->reveal();

    $session->setJsonWebTokens($tokens);

    self::assertEquals($tokens, $session->getJsonWebTokens());
  }

  /**
   * Tests the getJsonWebTokens() method.
   *
   * @covers \Drupal\oidc\OpenidConnectSession::setJsonWebTokens
   * @covers \Drupal\oidc\OpenidConnectSession::getJsonWebTokens
   */
  public function testGetJsonWebTokens() {
    $session_manager = $this->createSessionManager();

    $tokens_array = $this->getTokensArray();
    $tokens = $this->prophesize(JsonWebTokens::class);
    $tokens->toArray()->willReturn($tokens_array);

    $session = new OpenidConnectSession(
      $this->createOpenidConnectRealmManager(),
      $session_manager,
      $this->createAccount()
    );
    $session->setJsonWebTokens($tokens->reveal());

    $session = new OpenidConnectSession(
      $this->createOpenidConnectRealmManager(),
      $session_manager,
      $this->createAccount()
    );

    $tokens = $session->getJsonWebTokens();

    self::assertInstanceOf(JsonWebTokens::class, $tokens);
    self::assertEquals($tokens_array, $tokens->toArray());
  }

  /**
   * Tests the destroy() method.
   *
   * @covers \Drupal\oidc\OpenidConnectSession::destroy
   */
  public function testDestroy() {
    $session = new OpenidConnectSession(
      $this->createOpenidConnectRealmManager(),
      $this->createSessionManager([
        'plugin_id' => 'generic',
        'state' => 'abcdfghijklmnopqrstuvwxyz',
        'tokens' => $this->getTokensArray(),
      ]),
      $this->createAccount()
    );

    self::assertEquals('generic', $session->getRealmPluginId());
    self::assertNotEmpty('abcdfghijklmnopqrstuvwxyz', $session->getState());
    self::assertNotEmpty($session->getJsonWebTokens());

    $session->destroy();

    self::assertNull($session->getRealmPluginId());
    self::assertNull($session->getState());
    self::assertNull($session->getJsonWebTokens());
  }

  /**
   * Create an OpenID Connect realm manager prophecy.
   *
   * @return \Drupal\oidc\OpenidConnectRealm\OpenidConnectRealmManager
   *   The OpenID Connect realm manager prophecy.
   */
  protected function createOpenidConnectRealmManager() {
    return $this->prophesize(OpenidConnectRealmManager::class)->reveal();
  }

  /**
   * Create a session manager prophecy.
   *
   * @param array $default_attributes
   *   A list of default attributes.
   *
   * @return \Drupal\Core\Session\SessionManagerInterface
   *   The session manager prophecy.
   */
  protected function createSessionManager(array $default_attributes = ['plugin_id' => 'generic']) {
    $attributes_bag = $this->prophesize(AttributeBagInterface::class);

    $attributes_bag->has(Argument::type('string'))->willReturn(FALSE);
    $attributes_bag->get(Argument::type('string'), Argument::any())->will(function ($args) {
      return $args[1];
    });

    $attributes_bag->set(Argument::type('string'), Argument::any())->will(function ($args) use ($attributes_bag) {
      $attributes_bag->has($args[0])->willReturn(TRUE);
      $attributes_bag->get($args[0])->willReturn($args[1]);
      $attributes_bag->get($args[0], Argument::any())->willReturn($args[1]);
    });

    $attributes_bag->remove(Argument::type('string'))->will(function ($args) use ($attributes_bag) {
      $attributes_bag->has($args[0])->willReturn(FALSE);
      $attributes_bag->get($args[0])->willReturn(NULL);
      $attributes_bag->get($args[0], Argument::any())->will(function ($args) {
        return $args[1];
      });
    });

    if ($default_attributes) {
      foreach ($default_attributes as $name => $value) {
        $name = 'oidc_' . $name;

        $attributes_bag->has($name)->willReturn($value);
        $attributes_bag->get($name, Argument::any())->willReturn($value);
      }
    }

    $session_bag = $this->prophesize(SessionBagProxyInterface::class);
    $session_bag->isEmpty()->willReturn(FALSE);
    $session_bag->getBag()->willReturn($attributes_bag->reveal());

    $session_manager = $this->prophesize(SessionManagerInterface::class);
    $session_manager->getBag('attributes')->willReturn($session_bag->reveal());

    return $session_manager->reveal();
  }

  /**
   * Create an account prophecy.
   *
   * @param bool $authenticated
   *   Wether the account should be authenticated.
   *
   * @return \Drupal\Core\Session\AccountInterface
   *   The account prophecy.
   */
  protected function createAccount($authenticated = TRUE) {
    $account = $this->prophesize(AccountInterface::class);
    $account->isAuthenticated()->willReturn($authenticated);

    return $account->reveal();
  }

  /**
   * Get a JSON web tokens array.
   *
   * @return array
   *   The JSON web tokens.
   */
  protected function getTokensArray() {
    return [
      'type' => 'token type',
      'id_token' => ['value' => 'the id token', 'expires' => time() + 30],
      'access_token' => ['value' => 'the access token', 'expires' => time() + 30],
      'refresh_token' => ['value' => 'the refresh token', 'expires' => time() + 30],
      'claims' => [],
      'id_claim' => 'id',
      'username_claim' => 'username',
      'email_claim' => 'email',
      'given_name_claim' => 'given_name',
      'family_name_claim' => 'family_name',
    ];
  }

}
