<?php

namespace Drupal\Tests\oidc\Unit;

use Drupal\oidc\JsonHttp\JsonHttpGetRequest;
use Drupal\oidc\JsonHttp\JsonHttpPostRequest;
use Drupal\Tests\UnitTestCase;

/**
 * Tests the JSON HTTP request classes.
 *
 * @covers \Drupal\oidc\JsonHttp\JsonHttpRequest
 * @covers \Drupal\oidc\JsonHttp\JsonHttpGetRequest
 * @covers \Drupal\oidc\JsonHttp\JsonHttpPostRequest
 */
class JsonHttpRequestTest extends UnitTestCase {

  /**
   * Tests the getUrl() method.
   *
   * @covers \Drupal\oidc\JsonHttp\JsonHttpRequest::getUrl
   */
  public function testGetUrl() {
    $request = new JsonHttpGetRequest('https://example.com');
    self::assertEquals('https://example.com', $request->getUrl());

    $request = new JsonHttpGetRequest('https://example.com/path?query=abc');
    self::assertEquals('https://example.com/path?query=abc', $request->getUrl());
  }

  /**
   * Tests the getHeaders() method.
   *
   * @covers \Drupal\oidc\JsonHttp\JsonHttpRequest::getHeaders
   * @covers \Drupal\oidc\JsonHttp\JsonHttpRequest::addHeader
   */
  public function testGetHeaders() {
    $request = new JsonHttpGetRequest('https://example.com');

    self::assertEmpty($request->getHeaders());

    $request->addHeader('Secret-Token', 'secret token');

    self::assertEquals(['Secret-Token' => 'secret token'], $request->getHeaders());

    $request->addHeader('Secret-Key', 'secret key');

    self::assertEquals([
      'Secret-Token' => 'secret token',
      'Secret-Key' => 'secret key',
    ], $request->getHeaders());
  }

  /**
   * Tests the getBasicAuth() method.
   *
   * @covers \Drupal\oidc\JsonHttp\JsonHttpRequest::getBasicAuth
   * @covers \Drupal\oidc\JsonHttp\JsonHttpRequest::setBasicAuth
   */
  public function testGetBasicAuth() {
    $request = new JsonHttpGetRequest('https://example.com');

    self::assertNull($request->getBasicAuth());

    $request->setBasicAuth('my user', 'secret password');

    self::assertEquals(['my user', 'secret password'], $request->getBasicAuth());

    $request->setBasicAuth('my user 2', 'secret password 2');

    self::assertEquals(['my user 2', 'secret password 2'], $request->getBasicAuth());
  }

  /**
   * Tests the getFormParams() method.
   *
   * @covers \Drupal\oidc\JsonHttp\JsonHttpPostRequest::getFormParams
   * @covers \Drupal\oidc\JsonHttp\JsonHttpPostRequest::addFormParameter
   */
  public function testGetFormParams() {
    $request = new JsonHttpPostRequest('https://example.com');

    self::assertEmpty($request->getFormParams());

    $request->addFormParameter('param1', 'value 1');

    self::assertEquals(['param1' => 'value 1'], $request->getFormParams());

    $request->addFormParameter('param2', 'value b');

    self::assertEquals([
      'param1' => 'value 1',
      'param2' => 'value b',
    ], $request->getFormParams());
  }

}
