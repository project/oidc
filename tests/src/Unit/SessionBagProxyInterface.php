<?php

namespace Drupal\Tests\oidc\Unit;

use Symfony\Component\HttpFoundation\Session\SessionBagInterface;

/**
 * Provides the missing interface for \Symfony\Component\HttpFoundation\Session\SessionBagProxy so it can prophesized.
 */
interface SessionBagProxyInterface extends SessionBagInterface {

  /**
   * Get the bag.
   *
   * @return \Symfony\Component\HttpFoundation\Session\SessionBagInterface
   *   The session bag.
   */
  public function getBag();

  /**
   * Check if the bag is empy.
   *
   * @return bool
   *   Boolean indicating wether the bag is empty.
   */
  public function isEmpty();

}
