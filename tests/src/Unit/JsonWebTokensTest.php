<?php

namespace Drupal\Tests\oidc\Unit;

use Drupal\oidc\JsonWebTokens;
use Drupal\oidc\Token;
use Drupal\Tests\UnitTestCase;

/**
 * Tests the JsonWebTokens class.
 *
 * @covers \Drupal\oidc\JsonWebTokens
 */
class JsonWebTokensTest extends UnitTestCase {

  /**
   * Tests the fromArray() method and getters.
   *
   * @covers \Drupal\oidc\JsonWebTokens::getType
   * @covers \Drupal\oidc\JsonWebTokens::fromArray
   * @covers \Drupal\oidc\JsonWebTokens::getIdToken
   * @covers \Drupal\oidc\JsonWebTokens::getAccessToken
   * @covers \Drupal\oidc\JsonWebTokens::getRefreshToken
   * @covers \Drupal\oidc\JsonWebTokens::getClaims
   * @covers \Drupal\oidc\JsonWebTokens::getId
   * @covers \Drupal\oidc\JsonWebTokens::getUsername
   * @covers \Drupal\oidc\JsonWebTokens::getEmail
   * @covers \Drupal\oidc\JsonWebTokens::getGivenName
   * @covers \Drupal\oidc\JsonWebTokens::getFamilyName
   */
  public function testFromArrayAndGetters() {
    $claims = [
      'id' => '123',
      'username' => 'john.doe',
      'email' => 'john.doe@example.com',
      'given_name' => 'John',
      'family_name' => 'Doe',
    ];

    $tokens = JsonWebTokens::fromArray([
      'type' => 'token type',
      'id_token' => [
        'value' => 'the id token',
        'expires' => time() + 30,
      ],
      'access_token' => [
        'value' => 'the access token',
        'expires' => time() + 40,
      ],
      'refresh_token' => [
        'value' => 'the refresh token',
        'expires' => time() + 50,
      ],
      'claims' => $claims,
      'id_claim' => 'id',
      'username_claim' => 'username',
      'email_claim' => 'email',
      'given_name_claim' => 'given_name',
      'family_name_claim' => 'family_name',
    ]);

    self::assertEquals('token type', $tokens->getType());
    self::assertEquals('the id token', $tokens->getIdToken()->getValue());
    self::assertGreaterThan(time(), $tokens->getIdToken()->getExpires());
    self::assertEquals('the access token', $tokens->getAccessToken()->getValue());
    self::assertGreaterThan(time(), $tokens->getAccessToken()->getExpires());
    self::assertEquals('the refresh token', $tokens->getRefreshToken()->getValue());
    self::assertGreaterThan(time(), $tokens->getRefreshToken()->getExpires());
    self::assertEquals($claims, $tokens->getClaims());
    self::assertEquals($claims['id'], $tokens->getId());
    self::assertEquals($claims['username'], $tokens->getUsername());
    self::assertEquals($claims['email'], $tokens->getEmail());
    self::assertEquals($claims['given_name'], $tokens->getGivenName());
    self::assertEquals($claims['family_name'], $tokens->getFamilyName());
  }

  /**
   * Tests the fromArray() method and getters.
   *
   * @covers \Drupal\oidc\JsonWebTokens::getIdToken
   * @covers \Drupal\oidc\JsonWebTokens::setIdToken
   * @covers \Drupal\oidc\JsonWebTokens::getAccessToken
   * @covers \Drupal\oidc\JsonWebTokens::setAccessToken
   * @covers \Drupal\oidc\JsonWebTokens::getRefreshToken
   * @covers \Drupal\oidc\JsonWebTokens::setRefreshToken
   * @covers \Drupal\oidc\JsonWebTokens::clearRefreshToken
   */
  public function testTokenSettersAndGetters() {
    $id_token = new Token('id token', 30);
    $access_token = new Token('access token', 30);
    $refresh_token = new Token('refresh token', 30);
    $dummy_token = new Token('dummy token', 30);

    $tokens = new JsonWebTokens('token type', $id_token, $access_token, $refresh_token);

    self::assertEquals($id_token, $tokens->getIdToken());
    $tokens->setIdToken($dummy_token);
    self::assertEquals($dummy_token, $tokens->getIdToken());

    self::assertEquals($access_token, $tokens->getAccessToken());
    $tokens->setAccessToken($dummy_token);
    self::assertEquals($dummy_token, $tokens->getAccessToken());

    self::assertEquals($refresh_token, $tokens->getRefreshToken());
    $tokens->setRefreshToken($dummy_token);
    self::assertEquals($dummy_token, $tokens->getRefreshToken());
    $tokens->clearRefreshToken();
    self::assertNull($tokens->getRefreshToken());
  }

  /**
   * Tests the claim methods.
   *
   * @covers \Drupal\oidc\JsonWebTokens::getClaim
   * @covers \Drupal\oidc\JsonWebTokens::setClaim
   */
  public function testClaim() {
    $tokens = new JsonWebTokens('', new Token('', ''), new Token('', ''));

    $tokens->setClaim('given_name', 'John');
    self::assertEquals('John', $tokens->getClaim('given_name'));

    $tokens->setClaim('given_name', 'Jane');
    self::assertEquals('Jane', $tokens->getClaim('given_name'));
  }

  /**
   * Tests the ID claim.
   *
   * @covers \Drupal\oidc\JsonWebTokens::setClaim
   * @covers \Drupal\oidc\JsonWebTokens::setIdClaim
   * @covers \Drupal\oidc\JsonWebTokens::getId
   */
  public function testIdClaim() {
    $tokens = new JsonWebTokens('', new Token('', ''), new Token('', ''));
    $tokens->setClaim('id_1', '123');
    $tokens->setClaim('id_2', '456');

    $tokens->setIdClaim('id_1');
    self::assertEquals('123', $tokens->getId());

    $tokens->setIdClaim('id_2');
    self::assertEquals('456', $tokens->getId());
  }

  /**
   * Tests the username claim.
   *
   * @covers \Drupal\oidc\JsonWebTokens::setClaim
   * @covers \Drupal\oidc\JsonWebTokens::setUsernameClaim
   * @covers \Drupal\oidc\JsonWebTokens::getUsername
   */
  public function testUsernameClaim() {
    $tokens = new JsonWebTokens('', new Token('', ''), new Token('', ''));
    $tokens->setClaim('username_1', 'john.doe');
    $tokens->setClaim('username_2', 'jane.da');

    $tokens->setUsernameClaim('username_1');
    self::assertEquals('john.doe', $tokens->getUsername());

    $tokens->setUsernameClaim('username_2');
    self::assertEquals('jane.da', $tokens->getUsername());
  }

  /**
   * Tests the email claim.
   *
   * @covers \Drupal\oidc\JsonWebTokens::setClaim
   * @covers \Drupal\oidc\JsonWebTokens::setEmailClaim
   * @covers \Drupal\oidc\JsonWebTokens::getEmail
   */
  public function testEmailClaim() {
    $tokens = new JsonWebTokens('', new Token('', ''), new Token('', ''));
    $tokens->setClaim('email_1', 'john.doe@example.com');
    $tokens->setClaim('email_2', 'jane.da@example.com');

    $tokens->setEmailClaim('email_1');
    self::assertEquals('john.doe@example.com', $tokens->getEmail());

    $tokens->setEmailClaim('email_2');
    self::assertEquals('jane.da@example.com', $tokens->getEmail());
  }

  /**
   * Tests the given name claim.
   *
   * @covers \Drupal\oidc\JsonWebTokens::setClaim
   * @covers \Drupal\oidc\JsonWebTokens::setGivenNameClaim
   * @covers \Drupal\oidc\JsonWebTokens::getGivenName
   */
  public function testGivenNameClaim() {
    $tokens = new JsonWebTokens('', new Token('', ''), new Token('', ''));
    $tokens->setClaim('given_name_1', 'John');
    $tokens->setClaim('given_name_2', 'Jane');

    $tokens->setGivenNameClaim('given_name_1');
    self::assertEquals('John', $tokens->getGivenName());

    $tokens->setGivenNameClaim('given_name_2');
    self::assertEquals('Jane', $tokens->getGivenName());
  }

  /**
   * Tests the family name claim.
   *
   * @covers \Drupal\oidc\JsonWebTokens::setClaim
   * @covers \Drupal\oidc\JsonWebTokens::setFamilyNameClaim
   * @covers \Drupal\oidc\JsonWebTokens::getFamilyName
   */
  public function testFamilyNameClaim() {
    $tokens = new JsonWebTokens('', new Token('', ''), new Token('', ''));
    $tokens->setClaim('family_name_1', 'Doe');
    $tokens->setClaim('family_name_2', 'Da');

    $tokens->setFamilyNameClaim('family_name_1');
    self::assertEquals('Doe', $tokens->getFamilyName());

    $tokens->setFamilyNameClaim('family_name_2');
    self::assertEquals('Da', $tokens->getFamilyName());
  }

  /**
   * Tests the array transformations.
   *
   * @covers \Drupal\oidc\JsonWebTokens::fromArray
   * @covers \Drupal\oidc\JsonWebTokens::toArray
   */
  public function testArrayTransformations() {
    $data = [
      'type' => 'token type',
      'id_token' => 'the id token',
      'access_token' => 'the access token',
      'expires' => time() + 30,
      'claims' => [
        'id' => '123',
        'username' => 'john.doe',
        'email' => 'john.doe@example.com',
        'given_name' => 'John',
        'family_name' => 'Doe',
      ],
      'id_claim' => 'id',
      'username_claim' => 'username',
      'email_claim' => 'email',
      'given_name_claim' => 'given_name',
      'family_name_claim' => 'family_name',
    ];

    $tokens = JsonWebTokens::fromArray($data);

    self::assertEquals($data, $tokens->toArray());
  }

}
