<?php

namespace Drupal\Tests\oidc\Unit;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\externalauth\Event\ExternalAuthLoginEvent;
use Drupal\oidc\EventSubscriber\UpdateUserSubscriber;
use Drupal\oidc\JsonWebTokens;
use Drupal\oidc\OpenidConnectSessionInterface;
use Drupal\Tests\UnitTestCase;
use Drupal\user\UserInterface;
use Prophecy\Argument;

/**
 * Tests the UpdateUserSubscriber class.
 *
 * @covers \Drupal\oidc\EventSubscriber\ExternalAuthEventSubscriber
 */
class UpdateUserSubscriberTest extends UnitTestCase {

  /**
   * Tests the onLogin() method.
   *
   * @covers \Drupal\oidc\EventSubscriber\UpdateUserSubscriber::onLogin
   */
  public function testOnLogin() {
    $tokens = $this->prophesize(JsonWebTokens::class);
    $tokens->getUsername()->willReturn('john.doe');
    $tokens->getEmail()->willReturn('john.doe@example.com');
    $tokens->getGivenName()->willReturn('John');
    $tokens->getFamilyName()->willReturn('Doe');

    $session = $this->prophesize(OpenidConnectSessionInterface::class);
    $session->getJsonWebTokens()->willReturn($tokens->reveal());

    $storage = $this->prophesize(EntityStorageInterface::class);
    $storage = $storage->reveal();

    $entity_type_manager = $this->prophesize(EntityTypeManagerInterface::class);
    $entity_type_manager->getStorage(Argument::type('string'))->willReturn($storage);

    $user = $this->prophesize(UserInterface::class);
    $user->getAccountName()->willReturn('john.doe');
    $user->getEmail()->willReturn(NULL);
    $user->get(Argument::type('string'))->willReturn((object) ['value' => NULL]);
    $user->save()->willReturn(NULL);

    $user->setUsername(Argument::type('string'))->will(function ($args) use ($user) {
      $user->getAccountName()->willReturn($args[0]);
    });

    $user->setEmail(Argument::type('string'))->will(function ($args) use ($user) {
      $user->getEmail()->willReturn($args[0]);
    });

    $user->set(Argument::type('string'), Argument::any())->will(function ($args) use ($user) {
      $user->get($args[0])->willReturn((object) ['value' => $args[1]]);
    });

    $user = $user->reveal();

    $event = $this->prophesize(ExternalAuthLoginEvent::class);
    $event->getAccount()->willReturn($user);

    $external_auth_subscriber = new UpdateUserSubscriber($session->reveal(), $entity_type_manager->reveal());
    $external_auth_subscriber->onLogin($event->reveal());

    self::assertEquals('john.doe', $user->getAccountName());
    self::assertEquals('john.doe@example.com', $user->getEmail());
    self::assertEquals('John', $user->get('given_name')->value);
    self::assertEquals('Doe', $user->get('family_name')->value);
  }

}
