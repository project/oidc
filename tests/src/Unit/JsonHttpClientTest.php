<?php

namespace Drupal\Tests\oidc\Unit;

use Drupal\oidc\JsonHttp\JsonHttpClient;
use Drupal\oidc\JsonHttp\JsonHttpGetRequestInterface;
use Drupal\oidc\JsonHttp\JsonHttpPostRequestInterface;
use Drupal\Tests\UnitTestCase;
use GuzzleHttp\ClientInterface;
use Prophecy\Argument;
use Psr\Http\Message\MessageInterface;
use Psr\Http\Message\StreamInterface;

/**
 * Tests the JsonHttpClient class.
 *
 * @covers \Drupal\oidc\JsonHttp\JsonHttpClient
 */
class JsonHttpClientTest extends UnitTestCase {

  /**
   * Tests the get() method.
   *
   * @covers \Drupal\oidc\JsonHttp\JsonHttpClient::get
   */
  public function testGet() {
    $request = $this->prophesize(JsonHttpGetRequestInterface::class);
    $request->getUrl()->willReturn('https://example.com/page1');
    $request->getHeaders()->willReturn(['Secret' => 'abc']);
    $request->getBasicAuth()->willReturn(['my user', 'secret password']);

    $response = $this->createJsonHttpClient()->get($request->reveal());

    self::assertEquals('GET', $response[0]);
    self::assertEquals('https://example.com/page1', $response[1]);
    self::assertFalse($response[2]['allow_redirects']);
    self::assertEquals('application/json', $response[2]['headers']['Accept']);
    self::assertEquals('abc', $response[2]['headers']['Secret']);
    self::assertEquals('my user', $response[2]['auth'][0]);
    self::assertEquals('secret password', $response[2]['auth'][1]);

    $request->getUrl()->willReturn('https://example.com/page2');
    $request->getHeaders()->willReturn(['Not-Secret' => 'dfe']);
    $request->getBasicAuth()->willReturn(NULL);

    $response = $this->createJsonHttpClient()->get($request->reveal());

    self::assertEquals('GET', $response[0]);
    self::assertEquals('https://example.com/page2', $response[1]);
    self::assertEquals('dfe', $response[2]['headers']['Not-Secret']);
    self::assertArrayNotHasKey('auth', $response[2]);
  }

  /**
   * Tests the post() method.
   *
   * @covers \Drupal\oidc\JsonHttp\JsonHttpClient::post
   */
  public function testPost() {
    $request = $this->prophesize(JsonHttpPostRequestInterface::class);
    $request->getUrl()->willReturn('https://example.com');
    $request->getHeaders()->willReturn([]);
    $request->getBasicAuth()->willReturn(NULL);
    $request->getFormParams()->willReturn([
      'param1' => 'abc',
      'param2' => 'def',
    ]);

    $response = $this->createJsonHttpClient()->post($request->reveal());

    self::assertEquals('POST', $response[0]);
    self::assertEquals('abc', $response[2]['form_params']['param1']);
    self::assertEquals('def', $response[2]['form_params']['param2']);

    $request->getFormParams()->willReturn([
      'param1' => 'ghi',
    ]);

    $response = $this->createJsonHttpClient()->post($request->reveal());

    self::assertEquals('POST', $response[0]);
    self::assertEquals('ghi', $response[2]['form_params']['param1']);
    self::assertArrayNotHasKey('param2', $response[2]['form_params']);
  }

  /**
   * Create a JSON HTTP client.
   *
   * @return \Drupal\oidc\JsonHttp\JsonHttpClient
   *   The JSON HTTP client.
   */
  protected function createJsonHttpClient() {
    $stream = $this->prophesize(StreamInterface::class);
    $message = $this->prophesize(MessageInterface::class);

    $http_client = $this->prophesize(ClientInterface::class);
    $http_client->request(Argument::type('string'), Argument::type('string'), Argument::type('array'))->will(function ($args) use ($stream, $message) {
      $stream->getContents()->willReturn(json_encode($args));
      $message->getBody()->willReturn($stream->reveal());

      return $message->reveal();
    });

    return new JsonHttpClient($http_client->reveal());
  }

}
