Changelog
=========

All notable changes to the OpenID Connect Client module.


## 2.3.0 (2025-03-05):

### Added

* Drupal 11 compatibility (#3501943, #3503356)
* Setting to use a realm only for authentication

### Fixed

* Email settings hidden for no reason
* PHP 8.4 compatibility, implicitly marked as nullable parameters
* Not Found error for private files (#3500514)

### Removed

* Drupal 9 compatibility


## 2.2.2 (2024-06-14):

### Fixed

* Access denied on logout redirect if there's no end session endpoint

### Changed

* The given name and family name fields can now be configured in a view mode


## 2.2.1 (2024-01-09):

### Fixed

* Drupal 10.2 compatibility (#3412523)


## 2.2.0 (2023-06-29):

### Added

* Allow other modules to control which existing account should be linked (#3358166)

### Changed

* PHP 8.1+ is now required

### Fixed

* Missing return type for Drupal 10 compatibility (#3367977)
* Missing entity access check for Drupal 10 (#3368959)


## 2.1.0 (2023-05-10):

### Added

* Drupal 10 compatibility

### Changed

* Validate local cache if the config URL changed (#3309171)

### Fixed

* Broken pre uninstall hook (#3309171)
* Unnecessary requirement on "alg" property for keys (#3332643)
* 403 redirect being cached (#3304705)


## 2.0.0 (2022-05-31):

A new major release because various methods were refactored, breaking backwards compatibility in the API.

### Added

* Session expiration based on the access token lifetime
* Support for the refresh token
* Optional notification message when the session expired

### Changed

* Require External Authentication 2.x
* Renamed `oidc.general` config to `oidc.settings`

### Fixed

* Code style errors

### Removed

* Drupal 8 compatibility


## 1.2.0 (2022-05-26):

### Added

* Setting to prevent the user registration and password routes from being disabled (#3240582)

### Fixed

* Autocomplete not finding users that only have a given_name or family_name (#3250157)


## 1.1.1 (2021-09-15):

### Fixed

* Return state not being validated against the session


## 1.1.0 (2021-09-07):

This is a new minor version instead of patch release to fix a "release series" issue on Drupal.org.

### Added

* Extra condition to prevent linking with anonymous and user 1


## 1.0.3 (2021-08-26):

### Fixed

* Invalid hook name introduced in issue #3225383


## 1.0.2 (2021-08-26):

### Added

* Improved entity reference selection handlers for users

### Fixed

* Broken session during logout on Drupal 9.2


## 1.0.1 (2021-05-05):

### Fixed

* Broken 404 node pages due to kernel request subscriber


## 1.0.0 (2021-03-30):

### Fixed

* List indexes of generic realms

### Removed

* Requirement of drupal/core from composer.json


## 1.0.0-beta2 (2021-02-23):

### Fixed

* Fix 403 redirect being cached forever


## 1.0.0-beta1 (2021-02-23):

### Fixed

* Missing cacheability metadata when redirecting a 403


## 1.0.0-alpha3 (2021-01-28):

### Fixed

* Drupal 9 compatibility


## 1.0.0-alpha2 (2020-10-20):

### Added

* Helper trait to build the display name format options

### Fixed

* Deprecated `[user:name]` token replaced by `[user:account-name]`


## 1.0.0-alpha1 (2020-09-22):

First alpha release.
